document.addEventListener('DOMContentLoaded', event => {
  injectAnchors('.page-content');
  $('#toc').toc();
});

function injectAnchors(selector) {
  headings = $(selector)
    .find(':header')
    .each(function(index) {
      heading = $(this);
      heading_id = heading.attr('id');
      if (heading_id) {
        heading.append(
          ` <a href="#${heading_id}" class="heading-anchor"><i class="fas fa-link"></i></a>`
        );
      }
    });
}

$('.nav-sandwich').click(function(event) {
  event.preventDefault();
  $('.mobile-nav-menu').toggleClass('slide-up');
  $('.nav-sandwich').toggleClass('open');
  $('body').toggleClass('no-scroll');
});
